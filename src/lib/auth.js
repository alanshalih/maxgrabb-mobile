import axios from "axios"
import { Device } from '@capacitor/device';
class Auth{
 

    constructor(){
        let access_code = localStorage.getItem("maxgrabb:access_code")

        this.license = "trial"; 

        if(access_code)
        this.Register(access_code)

        let errorShow = ""
        
        
    }

    getLicense(){
        return this.license;
    }
 

    async Register(access_code){
        const Endpoint = "https://api.maxgrabb.com"

        const phoneId = await Device.getId();

        if(access_code)
        {
            try {
                const response = await axios.post(Endpoint + "/api/register-mobile-app", {
                    phoneId: phoneId.uuid,
                    access_code: access_code
                  })
    
                  if (response.data.access_code) {  
                    localStorage.setItem('maxgrabb:access_code',access_code) 
                    this.license = "full access";
                  }
 

                  return {data : "ok"};

            } catch (error) {
                return {data : "", error : error.response.data}
            }
        } 
    }
}

export default new Auth();