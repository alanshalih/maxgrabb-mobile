import Dexie from 'dexie';

export const db = new Dexie('MaxgrabbDB');
db.version(5).stores({
    lists: '++id, name', // Primary key and indexed props
    contacts : 'uid, title, wa_phone, list_id',
    campaigns : '++id,created_at'
});
